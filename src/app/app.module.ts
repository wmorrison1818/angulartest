import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/* Http client module */
import { HttpClientModule } from '@angular/common/http';

/* Service */
import { BugService } from './shared/joke.service';

/* Forms module */
import { ReactiveFormsModule } from '@angular/forms';

/* Components */
import { JokeListComponent } from './components/joke-list.component';

@NgModule({
  declarations: [
    AppComponent,
    JokeListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [BugService],
  bootstrap: [AppComponent]
})

export class AppModule { }