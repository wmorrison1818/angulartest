import { Component, OnInit } from '@angular/core';
import { BugService } from '../shared/joke.service';

@Component({
  selector: 'app-joke-list',
  templateUrl: './joke-list.component.html',
  styleUrls: ['../app.component.css']
})
export class JokeListComponent implements OnInit {

  jokeList: any = [];


  ngOnInit() {
    this.loadJokes();
  }

  constructor(
    public bugService: BugService
  ){ }

   loadJokes() {
    return this.bugService.GetJokes().subscribe((jokes: {}) => {
      this.jokeList = jokes;
    })
  }
}
